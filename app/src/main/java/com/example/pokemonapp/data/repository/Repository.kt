package com.example.pokemonapp.data.repository

import com.example.pokemonapp.data.api.RetrofitInstance
import com.example.pokemonapp.model.Pokemon
import retrofit2.Response

class Repository {

    suspend fun getPokeApi(): Response<Pokemon> {
        return RetrofitInstance.api.getPokemon()
    }

}