package com.example.pokemonapp.data.api

import com.example.pokemonapp.model.Pokemon
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("pokemon?limit=20&offset=0")
    suspend fun getPokemon(): Response<Pokemon>
}