package com.example.pokemonapp.model

data class Results(
    val name: String,
    val url: String
)