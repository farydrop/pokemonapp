package com.example.pokemonapp.screens.start

import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemonapp.R
import com.example.pokemonapp.model.Results
import kotlinx.android.synthetic.main.fragment_start.view.*


class StartFragment : Fragment() {

    lateinit var recyclerView: RecyclerView
    lateinit var adapter: StartAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewModel = ViewModelProvider(this).get(StartViewModel::class.java)
        val v = inflater.inflate(R.layout.fragment_start, container, false)
        recyclerView = v.rv_start
        adapter = StartAdapter(object: PokemonActionListener {
            override fun onPokemonDetails(result: Results) {
                val toast = Toast.makeText(activity, result.name,Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.TOP,0,160)
                toast.show()

                MyBottomSheetFragment()
                    .show(childFragmentManager, null)

            }
        })
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(context,2)
        viewModel.getPokeList()
        viewModel.myPokemonList.observe(viewLifecycleOwner) { list ->
            list.body()?.let { adapter.setlist(it.results) }
        }
        return v
    }
}