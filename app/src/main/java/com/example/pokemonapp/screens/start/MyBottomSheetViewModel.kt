package com.example.pokemonapp.screens.start

import android.os.Bundle
import android.renderscript.Element
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemonapp.R
import com.example.pokemonapp.data.repository.Repository
import com.example.pokemonapp.model.Pokemon
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.launch
import retrofit2.Response

class MyBottomSheetViewModel : ViewModel() {

    var repo = Repository()
    val myPokeList: MutableLiveData<Response<Pokemon>> = MutableLiveData()

    fun getPokemonList(){
        viewModelScope.launch {
            myPokeList.value = repo.getPokeApi()
        }
    }

}

