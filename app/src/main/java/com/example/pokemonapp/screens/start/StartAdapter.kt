package com.example.pokemonapp.screens.start

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemonapp.R
import com.example.pokemonapp.model.Results
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_pokemon_layout.view.*

interface PokemonActionListener {
    fun onPokemonDetails(result: Results)
}

class StartAdapter(private val actionListener: PokemonActionListener):
    RecyclerView.Adapter<StartAdapter.StartViewHolder>(),
    View.OnClickListener {

    private var listStart = emptyList<Results>()


    class StartViewHolder (view: View):RecyclerView.ViewHolder(view){
        var img_android: ImageView
        init {
            img_android =
                view.findViewById<View>(R.id.recyclerAvatarImageView) as ImageView
        }
    }

    override fun onClick(view: View) {
        val pokemon = view.tag as Results
        actionListener.onPokemonDetails(pokemon)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StartViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon_layout,parent,false)
        view.setOnClickListener(this)

        return StartViewHolder(view)
    }

    override fun onBindViewHolder(holder: StartViewHolder, position: Int) {
        val pokemon = listStart[position]
        holder.itemView.item_name.text = listStart[position].name

        var url = listStart[position].url
        if (url != null) {
            url = url.substring(34,url.lastIndexOf("/"))
        }
        Picasso.with(holder.img_android.context)
            .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/$url.png")
            .into(holder.img_android)

        holder.itemView.tag = pokemon
    }



    override fun getItemCount(): Int {
        return listStart.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setlist(list: List<Results>){
        listStart = list
        notifyDataSetChanged()
    }
}