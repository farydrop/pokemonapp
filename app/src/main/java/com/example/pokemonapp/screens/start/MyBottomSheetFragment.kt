package com.example.pokemonapp.screens.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemonapp.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.item_pokemon_layout.*

class MyBottomSheetFragment : BottomSheetDialogFragment() {

    val viewModel = MyBottomSheetViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottomsheet_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val tvTitle = view.findViewById<TextView>(R.id.poke_info)
        viewModel.getPokemonList()
        viewModel.myPokeList.observe(viewLifecycleOwner) { data ->
            data.body()?.let { tvTitle.text = it.results[1].name.toString() }
        }

    }
}
