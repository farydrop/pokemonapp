package com.example.pokemonapp.screens.start

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemonapp.data.repository.Repository
import com.example.pokemonapp.model.Pokemon
import kotlinx.coroutines.launch
import retrofit2.Response

class StartViewModel: ViewModel() {

    var repo = Repository()
    val myPokemonList: MutableLiveData<Response<Pokemon>> = MutableLiveData()

    fun getPokeList(){
        viewModelScope.launch {
            myPokemonList.value = repo.getPokeApi()
        }
    }

}